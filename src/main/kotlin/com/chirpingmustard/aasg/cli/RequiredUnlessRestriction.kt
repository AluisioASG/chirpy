/**
 * Copyright 2010 Cedric Beust <cedric@beust.com>
 * Copyright 2011 Dain Sundstrom <dain@iq80.com>
 * Copyright 2013 Michael Grove <mike@clarkparsia.com>
 * Copyright 2013 Fernando Hernandez <fernando@clarkparsia.com>
 * Copyright 2013-15 Rob Vesse <rvesse@dotnetrdf.org>
 * Copyright 2016 Rob Vesse <rvesse@dotnetrdf.org>
 * Copyright 2016 Aluísio Augusto Silva Gonçalves <aluisio@aasg.name>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.chirpingmustard.aasg.cli

import com.github.rvesse.airline.help.sections.HelpFormat
import com.github.rvesse.airline.help.sections.HelpHint
import com.github.rvesse.airline.model.OptionMetadata
import com.github.rvesse.airline.parser.ParseState
import com.github.rvesse.airline.parser.errors.ParseOptionMissingException
import com.github.rvesse.airline.restrictions.OptionRestriction
import com.github.rvesse.airline.restrictions.factories.OptionRestrictionFactory


class RequiredUnlessRestriction(vararg names: String) : OptionRestriction, HelpHint {
    private val names = names.toSet()

    override fun <T> postValidate(state: ParseState<T>, option: OptionMetadata) {
        if (names.isEmpty()) return

        // If this option was seen then the required criteria has been
        // fulfilled regardless of whether any of the triggering options
        // was actually present.
        if (state.parsedOptions.any { it.left == option }) return

        // Were any of the options that would relax the required
        // restriction present?
        val seen = state.parsedOptions.flatMap { it.left.options } .toSet()
        if (seen.intersect(names).isNotEmpty()) return

        // If we've reached this point, the option is still required
        // yet has not been specified.
        throw ParseOptionMissingException(option.title)
    }

    override fun <T> preValidate(state: ParseState<T>, option: OptionMetadata, value: String) {
        // No pre-validation
    }

    override fun getPreamble(): String? {
        return null
    }

    override fun getFormat(): HelpFormat {
        return HelpFormat.PROSE
    }

    override fun numContentBlocks(): Int {
        return 1
    }

    override fun getContentBlock(blockNumber: Int): Array<String> {
        if (blockNumber != 0) {
            throw IndexOutOfBoundsException()
        }
        return arrayOf("This option is required unless one of the following options are specified: ${this.names.joinToString(", ")}")
    }
}


@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FIELD)
annotation class RequiredUnless(val names: Array<String>)


class RequiredUnlessRestrictionFactory : OptionRestrictionFactory {
    override fun createOptionRestriction(annotation: Annotation): OptionRestriction? {
        if (annotation is RequiredUnless) {
            return RequiredUnlessRestriction(*annotation.names)
        }
        return null
    }

    override fun supportedOptionAnnotations(): List<Class<out Annotation>> {
        return listOf(RequiredUnless::class.java)
    }
}
