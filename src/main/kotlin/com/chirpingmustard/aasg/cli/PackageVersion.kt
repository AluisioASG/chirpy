package com.chirpingmustard.aasg.cli

import java.util.*


/**
 * Parse the manifest of a class' JAR file.
 *
 * @param[cls] a class in the JAR whose manifest is to be read.
 * @return [Properties] read from the manifest file.
 */
private fun readManifest(cls: Class<*>) = Properties().apply {
    cls.getResourceAsStream("/META-INF/MANIFEST.MF").use { load(it) }
}


/**
 * Returns the title of the specification implemented by a package.
 *
 * @param[cls] a class in the package for which to retrieve the title.
 * @return The specification title, or `null` if not known or readable.
 */
fun getSpecificationTitle(cls: Class<*>): String? =
        cls.`package`.specificationTitle ?: readManifest(cls).getProperty("Specification-Title")

/**
 * Returns the version of the specification implemented by a package.
 *
 * @param[cls] a class in the package for which to retrieve the version.
 * @return The specification version, or `null` if not known or readable.
 * @see java.lang.Package.getSpecificationVersion
 */
fun getSpecificationVersion(cls: Class<*>): String? =
        cls.`package`.specificationVersion ?: readManifest(cls).getProperty("Specification-Version")

/**
 * Returns the name of the implementation of a package.
 *
 * @param[cls] a class in the package for which to retrieve the name.
 * @return The implementation title, or `null` if not known or readable.
 */
fun getImplementationTitle(cls: Class<*>): String? =
        cls.`package`.implementationTitle ?: readManifest(cls).getProperty("Implementation-Title")

/**
 * Returns the version of the implementation of a package.
 *
 * @param[cls] a class in the package for which to retrieve the version.
 * @return The implementation version, or `null` if not known or readable.
 * @see java.lang.Package.getImplementationVersion
 */
fun getImplementationVersion(cls: Class<*>): String? =
        cls.`package`.implementationVersion ?: readManifest(cls).getProperty("Implementation-Version")
