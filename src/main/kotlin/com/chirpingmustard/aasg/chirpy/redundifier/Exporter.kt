package com.chirpingmustard.aasg.chirpy.redundifier

import com.chirpingmustard.aasg.chirpy.Database
import com.chirpingmustard.aasg.chirpy.Store
import com.chirpingmustard.aasg.chirpy.cli.ChirpyCommand
import com.chirpingmustard.aasg.cli.RequiredUnless
import com.github.rvesse.airline.annotations.Command
import com.github.rvesse.airline.annotations.Option
import com.github.rvesse.airline.annotations.restrictions.RequireOnlyOne
import me.tongfei.progressbar.ProgressBar
import org.basex.core.cmd.XQuery
import java.io.File
import java.io.InputStream
import java.nio.charset.Charset
import java.nio.file.Files
import java.nio.file.Path


/**
 * Reads the stream's contents as an UTF-8–encoded string.
 */
fun InputStream.readUTF8(): String {
    return use { readBytes().toString(Charset.forName("UTF-8")) }
}


object ObjectExporter {
    /** A serialized BBCode/XML-to-BBCode XSLT stylesheet. */
    val bbcodeStylesheet = this.javaClass.getResourceAsStream("bbcode.xsl").readUTF8()

    /**
     * Executes an export script.
     *
     * @param[what] The export script to execute, and so which kind of object to export.
     * @param[from] The store from which export the objects.
     * @param[to] File in which write the output of the export script.
     * @param[silent] Whether to display a progress bar or not.
     */
    operator fun invoke(what: String, from: Store, to: Path, silent: Boolean) {
        val script = this.javaClass.getResourceAsStream("export_$what.xq").readUTF8()

        ExportProgress.description = "Exporting $what"
        ExportProgress.silent = silent

        val oldOut = from.session.outputStream
        from.session.outputStream = Files.newOutputStream(to)
        val command = XQuery(script).apply {
            bind("bbcode-stylesheet-text", bbcodeStylesheet)
        }
        from.switchDatabase(Database.DATA)
        from.session.execute(command)
        ExportProgress.instance!!.stop()
        from.session.outputStream = oldOut
    }
}


@Command(name = "export")
class ExportCommand : ChirpyCommand() {
    @Option(name = arrayOf("-o", "--output"), description = "where to write the export file to")
    @RequiredUnless(arrayOf("--help", "--version"))
    private lateinit var output: File

    @Option(name = arrayOf("--users"), description = "export users")
    @RequireOnlyOne(tag = "export-type")
    protected @JvmField var users = false

    @Option(name = arrayOf("--messages"), description = "export messages")
    @RequireOnlyOne(tag = "export-type")
    protected @JvmField var messages = false


    override operator fun invoke() {
        // Check that an export type has been defined.  The command
        // line parser should handle this for us.
        val exportType = when {
            users -> "users"
            messages -> "messages"
            else -> throw AssertionError("no export type defined")
        }

        store.use {
            ObjectExporter(exportType, store, output.toPath(), silent)
        }
    }
}


/**
 * A no-argument constructable [ProgressBar].  The task name is set
 * statically.  It's also possible to silence the progress bar.
 */
class ExportProgress : ProgressBar(description, 0) {
    override fun start() {
        if (!silent) super.start()
    }

    override fun stop() {
        if (!silent) super.stop()
    }

    override fun maxHint(n: Int) {
        if (!silent) super.maxHint(n)
    }

    override fun step() {
        if (!silent) super.step()
    }


    /**
     * Sets the current instance as the single one.  Also starts the
     * progress bar.
     */
    fun setInstance() {
        instance = this
        this.start()
    }


    companion object {
        /** Task description. */
        var description: String = ""

        /** Whether to print the progress at all. */
        var silent: Boolean = false

        var instance: ExportProgress? = null
    }
}
