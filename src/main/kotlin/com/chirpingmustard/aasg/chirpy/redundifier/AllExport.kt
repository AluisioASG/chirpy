package com.chirpingmustard.aasg.chirpy.redundifier

import com.chirpingmustard.aasg.chirpy.cli.ChirpyCommand
import com.chirpingmustard.aasg.cli.RequiredUnless
import com.github.rvesse.airline.annotations.Command
import com.github.rvesse.airline.annotations.Option
import me.tongfei.progressbar.ProgressBar
import java.io.File
import java.io.PrintStream
import java.nio.file.Files


@Command(name = "regurgitate")
class AllExport : ChirpyCommand() {
    @Option(name = arrayOf("-H", "--avatar-salt"), description = "forum's avatar salt, to prefix filenames with")
    protected @JvmField var avatarSalt: String? = null

    @Option(name = arrayOf("-U", "--users-to"), description = "users export file")
    @RequiredUnless(arrayOf("--help", "--version"))
    private lateinit var usersTo: File

    @Option(name = arrayOf("-M", "--messages-to"), description = "messages export file")
    @RequiredUnless(arrayOf("--help", "--version"))
    private lateinit var messagesTo: File

    @Option(name = arrayOf("-S", "--avatars-sql-to"), description = "avatar enabling export file")
    @RequiredUnless(arrayOf("--help", "--version"))
    private lateinit var avatarsSqlTo: File

    @Option(name = arrayOf("-A", "--avatars-to"), description = "avatar directory")
    @RequiredUnless(arrayOf("--help", "--version"))
    private lateinit var avatarsTo: File


    override operator fun invoke() {
        store.use {
            ObjectExporter("users", store, usersTo.toPath(), silent)
            ObjectExporter("messages", store, messagesTo.toPath(), silent)

            val avatarOutDir = avatarsTo.toPath()
            val avatarSqlStream = PrintStream(avatarsSqlTo)
            Files.createDirectories(avatarOutDir)
            val progressBar = ProgressBar("Fetching avatars", 0)
            if (!silent) progressBar.start()
            val wait = facefy(store, avatarOutDir, avatarSqlStream, avatarSalt) { current, total ->
                if (!silent) {
                    progressBar.maxHint(total)
                    progressBar.step()
                }
            }
            wait()
            if (!silent) progressBar.stop()
        }
    }
}
