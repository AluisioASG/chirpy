package com.chirpingmustard.aasg.chirpy.redundifier

import com.chirpingmustard.aasg.chirpy.*
import com.chirpingmustard.aasg.chirpy.cli.ChirpyCommand
import com.chirpingmustard.aasg.cli.RequiredUnless
import com.github.rvesse.airline.annotations.Command
import com.github.rvesse.airline.annotations.Option
import me.tongfei.progressbar.ProgressBar
import okhttp3.Request
import org.jdom2.input.SAXBuilder
import java.io.File
import java.io.PrintStream
import java.io.StringReader
import java.net.URL
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.StandardCopyOption
import javax.imageio.ImageIO


/** A URL to signal end-of-input conditions. */
val SENTINEL_URL = URL("https://aasg.chirpingmustard.com/sfr/unchirper/EOF")


class AvatarFetcher : ThreadableTask<Pair<String, URL>, Pair<String, ByteArray>>(Pair("", SENTINEL_URL)) {
    /** The HTTP client used to fetch pages. */
    private val httpClient = newHttpClient(this.javaClass)

    /**
     * Fetches a named binary resource via HTTP.
     *
     * @param[input] a pair of resource ID and URL to fetch.
     * @return a pair with resource ID and a byte array for the body of the requested resource.
     */
    override operator fun invoke(input: Pair<String, URL>): Pair<String, ByteArray> {
        TransientError.wrap {
            logger.info("Fetching {}", input)
            val request = Request.Builder().url(input.second).build()
            val response = httpClient.newCall(request).execute()
            response.throwIfUnsuccessful()
            logger.debug("Fetch completed")
            return Pair(input.first, response.body().bytes())
        }
    }

    override fun close() = Unit
}


class AvatarIdentifier(val destDir: Path, val salt: String?) : ThreadableTask<Pair<String, ByteArray>, AvatarIdentifier.AvatarInfo>(Pair("", ByteArray(0))) {
    data class AvatarInfo(val userId: String, val filename: Path, val fileType: String, val width: Int, val height: Int) {
        override fun toString(): String {
            return "<Avatar user=$userId format=$fileType size=${width}x${height} filename=\"$filename\">"
        }
    }

    /**
     * Saves and describes an avatar.
     *
     * @param[input] a pair of user ID and avatar bytes.
     * @return a data object with information about the avatar and where it was saved.
     */
    override operator fun invoke(input: Pair<String, ByteArray>): AvatarIdentifier.AvatarInfo {
        TransientError.wrap {
            val baseFilename = destDir.resolve(input.first)
            logger.debug("Saving avatar of user {} to {}", input.first, baseFilename)
            Files.write(baseFilename, input.second)

            logger.debug("Identifying avatar of user {}", input.first)
            // This trick apparently comes from the Java™ Developers
            // Almanac by Patrick Chan, isbn:978-0-201-75280-9.
            val imageStream = ImageIO.createImageInputStream(baseFilename.toFile())
            val format = ImageIO.getImageReaders(imageStream).next().formatName
            val image = ImageIO.read(imageStream)

            val prefix = if (salt.isNullOrEmpty()) "" else "${salt}_"
            val finalFilename = destDir.resolve("$prefix${input.first}.${format.toLowerCase()}")
            Files.move(baseFilename, finalFilename, StandardCopyOption.REPLACE_EXISTING)
            return AvatarInfo(input.first, finalFilename, format.toLowerCase(), image.width, image.height).apply {
                logger.info(this)
            }
        }
    }

    override fun close() = Unit
}

class AvatarSQLWriter(val out: PrintStream) : ThreadableTask<AvatarIdentifier.AvatarInfo, Boolean>(AvatarIdentifier.AvatarInfo("", Paths.get(""), "", 0, 0)) {
    /**
     * Writes SQL commands to use an avatar.
     *
     * @param[input] information about the avatar.
     * @return `false`
     */
    override operator fun invoke(input: AvatarIdentifier.AvatarInfo): Boolean {
        out.println("""
            UPDATE phpbb_users
            SET user_avatar = '${input.userId}.${input.fileType}',
                user_avatar_type = 'avatar.driver.upload',
                user_avatar_width = ${input.width},
                user_avatar_height = ${input.height}
            WHERE user_id = ${input.userId};
        """.trimIndent())
        return false
    }

    override fun close() = Unit
}


/**
 * Fetches the avatars of authors with one and outputs SQL commands to
 * alter the forum's database to use them.
 *
 * Four extra threads are created: one to fetch the avatars, one to
 * analyze them, other to generate the SQL commands, and a last one
 * to verify and notify of the task's progress.
 *
 * @param[store] The store from which select the authors.
 * @param[avatarOut] Where to store the avatar files.
 * @param[sqlOut] Where to print the database change commands to.
 * @param[salt] The forum's avatar salt, if any.
 * @param[progressCallback] A function to keep track of the progress, receiving the number of completed and total steps.
 * @return a function to wait for the processing to finish.
 */
fun facefy(store: Store, avatarOut: Path, sqlOut: PrintStream, salt: String?, progressCallback: (Int, Int) -> Unit): () -> Unit {
    val xmlBuilder = SAXBuilder()
    val count = store.buildQuery(Database.DATA, "count(/author[exists(avatar)])").execute().toInt()

    val progressTask = ProgressReporter(count, progressCallback).thread(null)
    val exportTask = AvatarSQLWriter(sqlOut).thread(progressTask)
    val identifyTask = AvatarIdentifier(avatarOut, salt).thread(exportTask)
    val fetchTask = AvatarFetcher().thread(identifyTask)

    val query = store.buildQuery(Database.DATA, "/author[exists(avatar)]")
    while (query.more()) {
        val result = xmlBuilder.build(StringReader(query.next())).rootElement
        val userId = result.getAttributeValue("id").substring(1)
        val avatarUrl = result.getChildText("avatar")
        fetchTask.queue.add(Pair(userId, URL(avatarUrl)))
    }
    fetchTask.queue.add(fetchTask.task.sentinel)
    return {
        fetchTask.thread.join()
        identifyTask.thread.join()
        exportTask.thread.join()
        progressTask.thread.join()
    }
}


@Command(name = "fetch-faces")
class FetchAvatarCommand : ChirpyCommand() {
    @Option(name = arrayOf("-O", "--avatar-output-directory"), description = "where to save the avatar files to")
    @RequiredUnless(arrayOf("--help", "--version"))
    private lateinit var avatarOutDir: File
    @Option(name = arrayOf("-o", "--sql-output-file"), description = "where to write the database change commands to")
    @RequiredUnless(arrayOf("--help", "--version"))
    private lateinit var sqlOutFile: File
    @Option(name = arrayOf("-S", "--salt"), description = "forum's avatar salt, to prefix filenames with")
    protected @JvmField var salt: String? = null


    override operator fun invoke() {
        val avatarOutPath = avatarOutDir.toPath()
        val sqlOut = PrintStream(sqlOutFile)

        Files.createDirectories(avatarOutPath)

        val progressBar = ProgressBar("", 0)
        store.use {
            if (!silent) progressBar.start()
            val wait = facefy(store, avatarOutPath, sqlOut, salt) { current, total ->
                if (!silent) {
                    progressBar.maxHint(total)
                    progressBar.step()
                }
            }
            wait()
            if (!silent) progressBar.stop()
        }
    }
}
