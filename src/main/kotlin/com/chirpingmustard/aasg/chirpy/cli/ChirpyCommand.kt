package com.chirpingmustard.aasg.chirpy.cli

import com.chirpingmustard.aasg.chirpy.Store
import com.chirpingmustard.aasg.cli.RequiredUnless
import com.chirpingmustard.aasg.cli.getImplementationTitle
import com.chirpingmustard.aasg.cli.getImplementationVersion
import com.github.rvesse.airline.HelpOption
import com.github.rvesse.airline.annotations.Arguments
import com.github.rvesse.airline.annotations.Command
import com.github.rvesse.airline.annotations.Option
import com.github.rvesse.airline.help.Help
import com.github.rvesse.airline.model.GlobalMetadata
import java.io.PrintStream
import javax.inject.Inject


/** Version string builder. */
private object getVersionString {
    operator fun invoke(): String {
        val cls = this.javaClass
        return "${getImplementationTitle(cls)} version ${getImplementationVersion(cls)}"
    }
}


abstract class CommandBase {
    /**
     * Runs the command proper.
     */
    abstract operator fun invoke()
}


abstract class ChirpyCommand : CommandBase() {
    /** Flag for the `--help` standard command-line option. */
    @Inject
    lateinit var help: HelpOption<CommandBase>

    /** Flag for the `--version` standard command-line option. */
    @Option(name = arrayOf("-V", "--version"), description = "print version information and exit")
    protected @JvmField var version: Boolean = false

    /** Command-line flag for progress non-reporting. */
    @Option(name = arrayOf("-s", "-q", "--silent", "--quiet"), description = "don't print status messages")
    protected @JvmField var silent: Boolean = false

    /** Command-line parameter for the BaseX database URL. */
    @Option(name = arrayOf("-C", "--store"), description = "database to operate on")
    @RequiredUnless(arrayOf("--help", "--version"))
    protected lateinit var store: Store

    /**
     * Print the program version if the `--version` option was given.
     *
     * @param[dest] Stream to which write the version.
     * @return Whether the version was shown or not.
     */
    fun showVersionIfRequested(dest: PrintStream = System.out): Boolean {
        if (version) {
            dest.println(getVersionString())
        }
        return version
    }
}


/**
 * [ChirpyCommand] wrapper for Airline's [Help] command.
 */
@Command(name = "help", description = "display help information")
class HelpCommand : CommandBase() {
    @Inject
    private lateinit var cliMetadata: GlobalMetadata<CommandBase>

    /** No-op flag for the `--help` standard command-line option. */
    @Option(name = arrayOf("-h", "--help"), description = "print this help message and exit")
    protected @JvmField var help: Boolean = true

    /** Flag for the `--version` standard command-line option. */
    @Option(name = arrayOf("-V", "--version"), description = "print version information and exit")
    protected @JvmField var version: Boolean = false

    /** Command-line list of command names to show help for. */
    @Arguments(description = "commands for which show help")
    private var commands = mutableListOf<String>()

    /**
     * Displays help texts.
     */
    override operator fun invoke() {
        if (version) {
            // Display version info instead.
            println(getVersionString())
            return
        }

        Help.help(cliMetadata, commands, false)
    }
}
