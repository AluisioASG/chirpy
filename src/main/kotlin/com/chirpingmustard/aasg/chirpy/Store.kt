package com.chirpingmustard.aasg.chirpy

import cat.inspiracio.url.URL
import net.sf.saxon.s9api.XdmNode
import org.apache.logging.log4j.LogManager
import org.basex.api.client.ClientSession
import org.basex.api.client.LocalSession
import org.basex.api.client.Query
import org.basex.api.client.Session
import org.basex.core.Context
import org.basex.core.cmd.Close
import org.basex.core.cmd.Open
import org.basex.core.cmd.Optimize
import org.jdom2.Document
import org.jdom2.output.Format
import org.jdom2.output.XMLOutputter
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.Closeable
import java.io.InputStream


/**
 * Names of the SFR databases.
 *
 * @param[dbName] the database identifier, used for connection.
 */
enum class Database(val dbName: String) {
    /** The main database. */
    DATA("sfr"),
    /** The tasks database. */
    TASK("sfr-tasks"),
}


/**
 * A SFR store, abstracted over a connection to a BaseX server.
 *
 * @param[session] the database session.
 * @return a Store connected through the given session .
 */
class Store private constructor(val session: Session) : Closeable {
    private val logger = LogManager.getLogger(this.javaClass)

    /**
     * Connects to a remote store.
     *
     * @param[host] hostname of the machine at which the BaseX server is located.
     * @param[port] port at which the BaseX server is listening for connections.
     * @param[user] username to use to log in to the BaseX server.
     * @param[password] password of the database user.
     * @return a Store connected to the server at the given address as the given user.
     */
    constructor(host: String, port: Int, user: String, password: String) : this(ClientSession(host, port, user, password))

    /**
     * Connects to a local store.
     *
     * @param[user] username to use to log in to the BaseX server.
     * @param[password] password of the database user.
     * @return a Store connected to the local database as the given user.
     */
    constructor(user: String, password: String) : this(LocalSession(Context(), user, password))

    companion object {
        /**
         * Connects to a store with the connection information from a URL.
         *
         * If the URL contains no hostname, a local connection is made,
         * otherwise a client session is opened.
         *
         * @param[url] a server-based URI with connection details to a BaseX server, including user info.
         * @return a Store connected to the server at the given address.
         * @throws[IllegalArgumentException] if the URL does not include user info.
         */
        @JvmStatic
        fun valueOf(url: String): Store {
            val parsedUrl = URL(url)
            if (parsedUrl.username.isEmpty() || parsedUrl.password.isEmpty()) {
                throw IllegalArgumentException("URL contains no login credentials")
            }

            return if (parsedUrl.host.isEmpty()) {
                Store(parsedUrl.username, parsedUrl.password)
            } else {
                Store(parsedUrl.host, parsedUrl.port.toInt(), parsedUrl.username, parsedUrl.password)
            }
        }
    }

    /** The currently open database. */
    var currentDatabase: Database? = null

    /**
     * Fetches a task document from the store.
     *
     * @param[taskName] name of the task to fetch.
     * @return the serialized task document, or `null` if the task cannot be found.
     */
    fun fetchTask(taskName: String): InputStream? {
        logger.debug("Fetching task {}", taskName)
        val result = buildQuery(Database.TASK, "//task[@name=\$taskName]", mapOf(Pair("taskName", taskName))).execute()
        return if (result.isNotEmpty()) {
            result.byteInputStream()
        } else {
            logger.error("Task {} not found", taskName)
            null
        }
    }

    /**
     * Prepare a query for execution.
     *
     * Make sure to execute it before any other store function, or you
     * might end up using the wrong database.
     *
     * @param[db] The database on which run the query.
     * @param[query] The query to execute.
     * @param[params] A map of external parameters for the query, if any.
     * @return a query ready for execution or iteration.
     */
    fun buildQuery(db: Database, query: String, params: Map<String, *>? = null): Query {
        logger.debug("Building query \"{}\" against database {}", query, db)
        switchDatabase(db)
        val realQuery = StringBuilder()
        params?.forEach { p -> realQuery.append("declare variable \$${p.key} external;\n") }
        realQuery.append(query)
        val queryExe = session.query(realQuery.toString())
        params?.forEach { p -> queryExe.bind(p.key, p.value) }
        return queryExe
    }

    /**
     * Saves a serialized XML document into the store.
     *
     * @param[db] database in which save the document.
     * @param[path] path at which save the document.
     * @param[doc] the document to save.
     */
    fun save(db: Database, path: String, doc: InputStream) {
        logger.debug("Saving {} into {}", path, db)
        switchDatabase(db)
        session.replace(path, doc)
    }

    /**
     * Saves a serialized XML document into the store.
     *
     * @param[db] database in which save the document.
     * @param[path] path at which save the document.
     * @param[doc] the document to save.
     */
    fun save(db: Database, path: String, doc: ByteArray) = save(db, path, ByteArrayInputStream(doc))

    /**
     * Saves a Saxon document tree into the store.
     *
     * @param[db] database in which save the document.
     * @param[path] path at which save the document.
     * @param[doc] the document to save.
     */
    fun save(db: Database, path: String, doc: XdmNode) {
        throw NotImplementedError()
    }

    /**
     * Saves a JDOM2 document tree into the store.
     *
     * @param[db] database in which save the document.
     * @param[path] path at which save the document.
     * @param[doc] the document to save.
     */
    fun save(db: Database, path: String, doc: Document) {
        val out = ByteArrayOutputStream()
        XMLOutputter(Format.getCompactFormat().apply {
            omitDeclaration = true
        }).output(doc, out)
        save(db, path, out.toByteArray())
    }

    /**
     * Optimize a database.
     *
     * @param[db] the database to optimize.
     */
    fun optimize(db: Database) {
        logger.debug("Optimizing {}", db)
        switchDatabase(db)
        session.execute(Optimize())
    }

    /**
     * Switches to another database, if not the currently active one.
     *
     * @param[db] the database to open, or `null` to just close.
     */
    fun switchDatabase(db: Database?) {
        if (currentDatabase != db) {
            logger.trace("Switching active database from {} to {}", currentDatabase, db)
            session.execute(Close())
            if (db != null) session.execute(Open(db.dbName))
            currentDatabase = db
        }
    }

    override fun close() {
        logger.debug("Closing store")
        switchDatabase(null)
        session.close()
    }
}
