package com.chirpingmustard.aasg.chirpy

import net.sf.saxon.Configuration
import net.sf.saxon.lib.Logger
import net.sf.saxon.lib.OutputURIResolver
import net.sf.saxon.lib.Validation
import net.sf.saxon.s9api.Processor
import net.sf.saxon.s9api.XdmDestination
import net.sf.saxon.s9api.XsltTransformer
import nu.validator.htmlparser.common.XmlViolationPolicy
import nu.validator.htmlparser.sax.HtmlParser
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Level
import org.apache.logging.log4j.io.IoBuilder
import org.xml.sax.InputSource
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.io.OutputStream
import java.io.Reader
import java.util.*
import javax.xml.transform.Result
import javax.xml.transform.Source
import javax.xml.transform.sax.SAXSource
import javax.xml.transform.stream.StreamResult
import javax.xml.transform.stream.StreamSource


/**
 * SIMD data extractor from XML documents.
 *
 * [XSLTDataExtractor] assists in the generation of multiple data documents
 * derived from a collection of input documents through XSL transforms.
 *
 * @param[styleSheetName] name of the style sheet resource used to process each input document.
 * @param[allowDuplicates] whether to allow the transformer to overwrite its own result documents.
 */
class XSLTDataExtractor(val styleSheetName: String, val allowDuplicates: Boolean = false) {
    private val logger = LogManager.getLogger(this.javaClass)
    /** The stylesheet entry point. */
    private val styleSheet: XsltTransformer
    /** A throwaway sink for the stylesheet's main output (all objects
     *  are output in other result documents). */
    private val throwawayDestination = XdmDestination()


    init {
        val processor = Processor(Configuration().apply {
            // Bridge Saxon's logger into ours.
            logger = object : Logger() {
                val realLogger = LogManager.getLogger(this.javaClass)
                val outStream : OutputStream by lazy {
                    IoBuilder.forLogger(realLogger)
                    .setLevel(Level.DEBUG)
                    .buildOutputStream()
                }

                override fun println(message: String, severity: Int) {
                    val level = when (severity) {
                        DISASTER -> Level.FATAL
                        ERROR -> Level.ERROR
                        INFO -> Level.INFO
                        WARNING -> Level.WARN
                        else -> throw AssertionError("Unknown message severity $severity")
                    }
                    realLogger.log(level, message)
                }

                override fun asStreamResult() = StreamResult(outStream)

                override fun close() {
                    outStream.close()
                }
            }
            // Ignore errors in the input.
            parseOptions.dtdValidationMode = Validation.LAX
        })
        // Now we just have to retrieve and compile the stylesheet.
        val compiler = processor.newXsltCompiler()
        val styleSheetSource = StreamSource(this.javaClass.getResourceAsStream(styleSheetName), this.javaClass.getResource(styleSheetName).toString())
        styleSheet = compiler.compile(styleSheetSource).load()
    }

    /**
     * Extracts data from an HTML page through the stylesheet.
     *
     * Data objects are expected to be output via result documents.
     * The stylesheet's main output is discarded.
     *
     * @param[body] the body of the HTML page, as a named XML source (use [sourceInput] for proper parsing).
     * @return A map of result document names to their serialized contents.
     */
    fun transform(body: Source): Map<String, ByteArray> {
        // Prepare the inputs and outputs.
        throwawayDestination.reset()
        val resultStore = MemoryXSLTOutput(allowDuplicates)
        // Configure the respective parameters of the stylesheet.
        styleSheet.underlyingController.outputURIResolver = resultStore
        styleSheet.setSource(body)
        styleSheet.destination = throwawayDestination
        // Then go and reap the awards.
        styleSheet.transform()
        return resultStore.outputs()
    }

    /**
     * Maps a named byte stream into a XML [Source].
     *
     * @param[stream] the serialized XML input stream.
     * @param[systemId] the stream's name.
     * @return A [Source] configured to parse the input stream.
     */
    fun sourceInput(stream: InputStream, systemId: String): Source {
        val inputReader = HtmlParser(XmlViolationPolicy.ALLOW)
        return SAXSource(inputReader, InputSource(stream).apply { this.systemId = systemId })
    }

    /**
     * Maps a named character stream into a XML [Source].
     *
     * @param[stream] the serialized XML input stream.
     * @param[systemId] the stream's name.
     * @return A [Source] configured to parse the input stream.
     */
    fun sourceInput(stream: Reader, systemId: String): Source {
        val inputReader = HtmlParser(XmlViolationPolicy.ALLOW)
        return SAXSource(inputReader, InputSource(stream).apply { this.systemId = systemId })
    }
}


/**
 * An in-memory store for XSLT result documents.
 *
 * [MemoryXSLTOutput] allows XSLT documents to produce result documents
 * that are not outright persisted, useful for example if there is
 * additional processing to be done or if the documents are to be sent
 * through the network rather than stored.
 *
 * @param[allowDuplicates] whether to allow the transformer to output to the same result document (preventing XTDE1490 to occur).  Note that this does not result in all results being stored; rather, only the last document generated for a given path is kept.
 */
private class MemoryXSLTOutput(val allowDuplicates: Boolean = false) : OutputURIResolver {
    /** Map of [results][Result] to the corresponding output names and streams. */
    private val pending: MutableMap<Result, Pair<String, ByteArrayOutputStream>> = HashMap()
    /** Map of ready documents. */
    private val complete: MutableMap<String, ByteArray> = HashMap()

    override fun newInstance(): OutputURIResolver {
        return this
    }

    override fun resolve(href: String, base: String): Result? {
        val out = ByteArrayOutputStream()
        val result = StreamResult(out)
        // The OutputURIResolver docs say that “setting the systemId
        // to null […] will defeat these error checks” (wrt. double
        // destination).  Guess what?  It doesn't, they check for it
        // and resolve the URI themselves if it is null.
        result.systemId = if (allowDuplicates) UUID.randomUUID().toString() else null
        pending.put(result, Pair(href, out))
        return result
    }

    override fun close(result: Result) {
        val (href, out) = pending.remove(result)!!
        complete.put(href, out.toByteArray())
    }

    /**
     * Checks whether all pending documents have finished being generated.
     *
     * @return `true` if there are no documents pending closing.
     */
    fun isReady(): Boolean = pending.isEmpty()

    /**
     * Returns all result documents generated by the transformer.
     *
     * Make sure the transformer has finished generating documents
     * (with [isReady]) before calling this function.
     *
     * @return A map of result documents' names to their serialized bodies.
     */
    fun outputs(): Map<String, ByteArray> {
        assert(isReady())
        return complete
    }
}
