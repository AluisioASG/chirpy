package com.chirpingmustard.aasg.chirpy

import com.chirpingmustard.aasg.cli.getImplementationTitle
import com.chirpingmustard.aasg.cli.getImplementationVersion
import okhttp3.OkHttpClient
import okhttp3.Response


/**
 * A HTTP request resulted in an unsuccessful response.
 */
class UnsuccessfulHttpResponseException(val response: Response) : Exception("Got HTTP status code ${response.code()} for ${response.request().url()}: ${response.message()}") {
    val code = response.code()
}


/**
 * Throws an [UnsuccessfulHttpResponseException] if the response code is not successful.
 *
 * @see [Response.isSuccessful]
 */
fun Response.throwIfUnsuccessful() {
    if (!this.isSuccessful) {
        throw UnsuccessfulHttpResponseException(this)
    }
}


/**
 * Creates a new HTTP client for the calling class.
 *
 * @param[caller] the class which is going to use the client.
 * @return a HTTP client.
 */
fun newHttpClient(caller: Class<*>): OkHttpClient {
    val userAgent = "${getImplementationTitle(caller)}/${getImplementationVersion(caller)} (+https://aasg.chirpingmustard.com/)"

    // From https://stackoverflow.com/a/27840834
    return OkHttpClient.Builder().addNetworkInterceptor { chain ->
        val originalRequest = chain.request()
        val requestWithUserAgent = originalRequest.newBuilder()
                .header("User-Agent", userAgent)
                .build()
        chain.proceed(requestWithUserAgent)
    }.build()
}
