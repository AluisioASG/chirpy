package com.chirpingmustard.aasg.chirpy.feeder

import com.chirpingmustard.aasg.chirpy.cli.ChirpyCommand
import com.chirpingmustard.aasg.cli.RequiredUnless
import com.github.rvesse.airline.annotations.Arguments
import com.github.rvesse.airline.annotations.Command
import com.github.rvesse.airline.annotations.Option
import me.tongfei.progressbar.ProgressBar
import java.net.URL


/**
 * The "run" command.  Fetches pages and stores them into the database.
 */
@Command(name = "feed")
class RunCommand : ChirpyCommand() {
    @Option(name = arrayOf("-O", "--optimize"), description = "optimize the database after update")
    protected @JvmField var optimize: Boolean = false

    @Arguments(description = "pages to fetch")
    @RequiredUnless(arrayOf("--help", "--version"))
    private lateinit var urls: List<URL>


    override fun invoke() {
        var progressBar = ProgressBar("", urls.size)
        store.use {
            if (!silent) progressBar.start()
            val wait = unchirp(urls, store, optimize) { current, total ->
                if (!silent) progressBar.step()
            }
            wait()
            if (!silent) progressBar.stop()
        }
    }
}
