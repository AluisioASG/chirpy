package com.chirpingmustard.aasg.chirpy.feeder

import com.chirpingmustard.aasg.chirpy.*
import okhttp3.Request
import java.io.Reader
import java.io.StringReader
import java.net.URL


/** A URL to signal end-of-input conditions. */
val SENTINEL_URL = URL("https://aasg.chirpingmustard.com/sfr/unchirper/EOF")


class Fetcher : ThreadableTask<URL, Pair<URL, Reader>>(SENTINEL_URL) {
    /** The HTTP client used to fetch pages. */
    private val httpClient = newHttpClient(this.javaClass)

    /**
     * Fetches a text resource via HTTP.
     *
     * @param[input] URL of the resource to fetch.  Redirects are followed.
     * @return a pair with the final URL and a character stream for the body of the requested resource.
     */
    override operator fun invoke(input: URL): Pair<URL, Reader> {
        TransientError.wrap {
            logger.info("Fetching {}", input)
            val request = Request.Builder().url(input).build()
            val response = httpClient.newCall(request).execute()
            response.throwIfUnsuccessful()
            logger.debug("Fetch completed")
            return Pair(response.request().url().url(), response.body().charStream())
        }
    }

    override fun close() = Unit
}


class Extractor(val styleSheet: String, val overwriteDuplicateOutputs: Boolean) : ThreadableTask<Pair<URL, Reader>, Map<String, ByteArray>>(Pair(SENTINEL_URL, StringReader(""))) {
    /** The XSL transform runner we use to extract data from the pages. */
    private val extractor = XSLTDataExtractor(styleSheet, overwriteDuplicateOutputs)

    /**
     * Extracts data objects from a XML/HTML web document.
     *
     * @param[inputUrl] The source URL of the document being processed.
     * @param[doc] A character-serialized form of the input document.
     * @return a map of extracted object names and their serialized XML representations.
     */
    operator fun invoke(inputUrl: URL, doc: Reader): Map<String, ByteArray> {
        logger.info("Extracting objects from {}", inputUrl)
        TransientError.wrap {
            val source = extractor.sourceInput(doc, inputUrl.toString())
            return extractor.transform(source)
        }
    }

    override operator fun invoke(input: Pair<URL, Reader>): Map<String, ByteArray> = this(input.first, input.second)

    override fun close() = Unit
}


class Updater(val store: Store, val optimizeOnClose: Boolean) : ThreadableTask<Map<String, ByteArray>, Boolean>(hashMapOf()) {
    /** Whether the updater has been closed already or not. */
    private var closed = false

    /**
     * Saves a XML document into the store.
     *
     * The document is identified through the given path.  If another
     * document already exists with that path, it is replaced.
     *
     * @param[path] Store path at which store the document.
     * @param[doc] Serialized XML document to store.
     */
    operator fun invoke(path: String, doc: ByteArray) {
        if (closed) {
            throw logger.throwing(IllegalStateException("Updater has already been closed"))
        }
        logger.debug("Saving {}", path)
        store.save(Database.DATA, path, doc)
    }

    /**
     * Saves a collection of XML documents into the store.
     *
     * @param[input] A mapping of document names to their XML serializations.
     * @return `false`
     */
    override operator fun invoke(input: Map<String, ByteArray>): Boolean {
        logger.info("Updating database")
        for ((path, doc) in input) {
            this(path, doc)
        }
        return false
    }

    /**
     * Terminates the updater.
     *
     * The [optimizeOnClose] property determines whether the store
     * database is optimized before closing.
     *
     * The underlying store is **not** closed with the updater.
     * You may continue to use it or close it yourself afterwards.
     */
    override fun close() {
        if (optimizeOnClose) {
            logger.info("Optimizing database")
            store.optimize(Database.DATA)
        }
        closed = true
    }
}


/**
 * Feeds the store with pages in a multithreaded configuration.
 *
 * Four extra threads are created: one to fetch the pages, one to
 * parse them and extract data, other to update the database, and
 * a last one to verify and notify of the task's progress.
 *
 * @param[urls] The URLs of the pages to feed the database.
 * @param[store] The store to feed.
 * @param[optimize] Whether to optimize the store database after updating it.
 * @param[progressCallback] A function to keep track of the progress, receiving the number of completed and total steps.
 * @return a function to wait for the processing to finish.
 */
fun unchirp(urls: Collection<URL>, store: Store, optimize: Boolean, progressCallback: (Int, Int) -> Unit): () -> Unit {
    val progressTask = ProgressReporter(urls.size, progressCallback).thread(null)
    val updateTask = Updater(store, optimize).thread(progressTask)
    val extractionTask = Extractor("feeder/np.xsl", true).thread(updateTask)
    val fetchTask = Fetcher().thread(extractionTask)

    fetchTask.queue.addAll(urls)
    fetchTask.queue.add(fetchTask.task.sentinel)
    return {
        fetchTask.thread.join()
        extractionTask.thread.join()
        updateTask.thread.join()
        progressTask.thread.join()
    }
}
