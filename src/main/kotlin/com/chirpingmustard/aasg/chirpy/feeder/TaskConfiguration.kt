package com.chirpingmustard.aasg.chirpy.feeder

import com.chirpingmustard.aasg.chirpy.CommandLine
import com.chirpingmustard.aasg.chirpy.Database
import com.chirpingmustard.aasg.chirpy.cli.ChirpyCommand
import com.chirpingmustard.aasg.cli.RequiredUnless
import com.github.rvesse.airline.annotations.Command
import com.github.rvesse.airline.annotations.Option
import org.jdom2.Document
import org.jdom2.input.SAXBuilder
import org.jdom2.output.Format
import org.jdom2.output.XMLOutputter
import org.jonnyzzz.kotlin.xml.dsl.jdom.jdom
import java.net.URL


/**
 * The "configure-task" command.  Configures tasks for stateful fetching.
 */
@Command(name = "setup-autofeed")
class ConfigureTaskCommand : ChirpyCommand() {
    @Option(name = arrayOf("-N", "--name"), description = "name of the task to configure")
    @RequiredUnless(arrayOf("--help", "--version"))
    private lateinit var taskName: String

    @Option(name = arrayOf("-T", "--thread-url"), description = "base thread URL")
    protected @JvmField var threadUrl = URL("http://forums.xkcd.com/viewtopic.php?t=101043")
    @Option(name = arrayOf("-P", "--posts-per-page"), description = "default number of posts per page in the thread")
    protected @JvmField var postsPerPage = 40
    @Option(name = arrayOf("-o", "--backoff-previous"), description = "number of pages to rewind from the last run")
    protected @JvmField var backoffPrevious = -1
    @Option(name = arrayOf("-p", "--backoff-latest"), description = "distance, in pages, to stay away from the latest page")
    protected @JvmField var backoffLatest = 0
    @Option(name = arrayOf("-R", "--loop"), arity = 1, description = "return to the first page after catching up with the thread")
    protected @JvmField var loop = false
    @Option(name = arrayOf("-m", "--max-pages"), description = "maximum number of pages to download each run")
    protected @JvmField var maxPages = 1

    @Option(name = arrayOf("--override-last-page"), description = "override the last page fetched")
    protected @JvmField var lastPageOverride: Int? = null


    override operator fun invoke() {
        store.use {
            val taskData = store.fetchTask(taskName)?.let { SAXBuilder().build(it) }
            // Check whether we need to modify something or just show it.
            if (setOf("thread-url", "posts-per-page", "backoff-previous", "backoff-latest",
                      "loop", "max-pages", "override-last-page")
                .none { CommandLine.has("--${it}") }) {
                showConfig(taskName, taskData)
            } else {
                val newData = configure(taskName, taskData)
                store.save(Database.TASK, taskName, newData)
            }
        }
    }

    /**
     * Fetch the config and state of a task and print them to the standard output stream.
     *
     * @param[taskName] name of the task to display.
     * @param[taskData] stored data for the task, if any.
     */
    fun showConfig(taskName: String, taskData: Document?) {
        if (taskData == null) {
            println("Task '$taskName' not found.")
        } else {
            println("Configuration and state for task '$taskName':")
            XMLOutputter(Format.getPrettyFormat().apply {
                omitDeclaration = true
            }).output(taskData, System.out)
        }
    }

    /**
     * Update a task.
     *
     * @param[taskName] name of the task to update.
     * @param[taskData] stored data for the task, if any.
     * @return the task's updated data document.
     */
    fun configure(taskName: String, taskData: Document?): Document {
        return if (taskData == null) {
            // Build a new task config.
            println("Configuring task '$taskName'")
            Document(jdom("task") {
                attribute("name", taskName)
                element("config") {
                    element("thread-url") { text(threadUrl.toString()) }
                    element("posts-per-page") { text(postsPerPage.toString()) }
                    element("backoff-previous") { text(backoffPrevious.toString()) }
                    element("backoff-latest") { text(backoffLatest.toString()) }
                    element("max-pages") { text(maxPages.toString()) }
                    if (loop) {
                        element("loop")
                    }
                }
                element("state") {
                    element("last-page") { text((lastPageOverride ?: 0).toString()) }
                }
            })
        } else {
            // Alter the current task config based on the command-line options.
            println("Reconfiguring task '$taskName'")
            val config = taskData.rootElement.getChild("config")
            val state = taskData.rootElement.getChild("state")

            if (CommandLine.has("--thread-url")) {
                config.getChild("thread-url").text = threadUrl.toString()
            }
            if (CommandLine.has("--posts-per-page")) {
                config.getChild("posts-per-page").text = postsPerPage.toString()
            }
            if (CommandLine.has("--backoff-previous")) {
                config.getChild("backoff-previous").text = backoffPrevious.toString()
            }
            if (CommandLine.has("--backoff-latest")) {
                config.getChild("backoff-latest").text = backoffLatest.toString()
            }
            if (CommandLine.has("--max-pages")) {
                config.getChild("max-pages").text = maxPages.toString()
            }
            if (CommandLine.has("--loop")) {
                if (loop && config.getChild("loop") == null) {
                    config.addContent(jdom("loop"))
                } else if (!loop && config.getChild("loop") != null) {
                    config.removeChild("loop")
                }
            }

            if (lastPageOverride != null) {
                state.getChild("last-page").text = lastPageOverride.toString()
            }

            taskData
        }
    }
}
