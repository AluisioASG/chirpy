package com.chirpingmustard.aasg.chirpy.feeder

import com.chirpingmustard.aasg.chirpy.Database
import com.chirpingmustard.aasg.chirpy.Store
import com.chirpingmustard.aasg.chirpy.XSLTDataExtractor
import com.chirpingmustard.aasg.chirpy.cli.ChirpyCommand
import com.chirpingmustard.aasg.cli.RequiredUnless
import com.chirpingmustard.aasg.chirpy.newHttpClient
import com.github.rvesse.airline.annotations.Command
import com.github.rvesse.airline.annotations.Option
import me.tongfei.progressbar.ProgressBar
import okhttp3.HttpUrl
import okhttp3.Request
import org.jdom2.Element
import org.jdom2.input.SAXBuilder
import java.net.URL
import java.nio.charset.Charset


/**
 * The "run-task" command.  Execute tasks according to their config.
 */
@Command(name = "autofeed")
class RunTaskCommand : ChirpyCommand() {
    /** The XSL transform runner we use to extract the page numbers from a thread. */
    private val pageNumberExtractor = XSLTDataExtractor("feeder/thread-pages.xsl")
    /** The HTTP client for use when fetching pages. */
    private val httpClient = newHttpClient(this.javaClass)

    @Option(name = arrayOf("-N", "--name"), description = "name of the task to execute")
    @RequiredUnless(arrayOf("--help", "--version"))
    private lateinit var taskName: String

    @Option(name = arrayOf("-O", "--optimize"), description = "optimize the database after update")
    protected @JvmField var optimize: Boolean = false

    @Option(name = arrayOf("--override-last-page"), description = "override the last page fetched")
    protected @JvmField var lastPageOverride: Int? = null


    override operator fun invoke() {
        store.use {
            val taskData = store.fetchTask(taskName)
            if (taskData == null) {
                System.err.println("Task '$taskName' not found.")
                return@use
            }
            val taskDoc = SAXBuilder().build(taskData)
            val config = taskDoc.rootElement.getChild("config")
            val state = taskDoc.rootElement.getChild("state")

            if (lastPageOverride != null) {
                state.getChild("last-page").text = lastPageOverride.toString()
            }

            if (!silent) println("Running task '$taskName'\u2026")
            runTask(store, taskName, config, state)
            store.save(Database.TASK, taskName, taskDoc)
        }
    }

    /**
     * Execute a task.
     *
     * @param[store] the database store.
     * @param[name] name of the task.
     * @param[config] the task config.
     * @param[state] the task's live state.
     */
    fun runTask(store: Store, name: String, config: Element, state: Element) {
        val threadUrl = URL(config.getChildText("thread-url"))
        val backoffPrevious = config.getChildText("backoff-previous").toInt()
        val backoffLatest = config.getChildText("backoff-latest").toInt()
        val maxPages = config.getChildText("max-pages").toInt()
        val postsPerPage = config.getChildText("posts-per-page").toInt()
        val loop = config.getChild("loop") != null

        val lastPage = state.getChildText("last-page").toInt()

        val threadPage = Request.Builder().url(threadUrl).build().let {
            httpClient.newCall(it).execute().body().charStream()
        }.let { pageNumberExtractor.sourceInput(it, threadUrl.toString()) }
        val latestPage = pageNumberExtractor.transform(threadPage)["latest"]!!.toString(Charset.forName("UTF-8")).toInt()
        val nextPage = Math.max(Math.min(lastPage - backoffPrevious, latestPage - backoffLatest), 1)
        val finalPage = Math.min(if (maxPages != 0) nextPage + maxPages - 1 else Int.MAX_VALUE,
                                 latestPage - backoffLatest)

        val threadUrlBuilder = HttpUrl.parse(threadUrl.toString()).newBuilder()
        val urls = (nextPage..finalPage).map {
            val start = (it - 1) * postsPerPage
            threadUrlBuilder.setQueryParameter("start", start.toString()).build().url()
        }

        var progressBar = ProgressBar(name, urls.size)
        store.use {
            if (!silent) progressBar.start()
            val wait = unchirp(urls, store, optimize) { current, total ->
                if (!silent) progressBar.step()
            }
            wait()
            if (!silent) progressBar.stop()
        }

        state.getChild("last-page").text = if (finalPage == latestPage && loop) "0" else finalPage.toString()
    }
}
