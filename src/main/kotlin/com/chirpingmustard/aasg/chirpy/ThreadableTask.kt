package com.chirpingmustard.aasg.chirpy

import org.apache.logging.log4j.LogManager
import java.io.Closeable
import java.util.concurrent.BlockingQueue
import java.util.concurrent.LinkedBlockingQueue
import kotlin.concurrent.thread


/** Returns the class' base name, sans package. */
private fun<T> Class<T>.baseName() = name.substringAfterLast(".")


/**
 * A [ThreadableTask] spun off its own thread.
 *
 * @param[task] The [ThreadableTask] this corresponds to.
 * @param[thread] The [Thread] the task is being run in.
 * @param[queue] The input queue for the task.
 */
class ThreadedTask<I, O>(val task: ThreadableTask<I, O>, val thread: Thread, val queue: BlockingQueue<I>)


/**
 * A task that can be run in its own thread for pipelined execution.
 *
 * @param[sentinel] A non-valid input to the task, used to signal end of input.
 */
abstract class ThreadableTask<I, O>(val sentinel: I) : Closeable {
    /** A logger for the task. */
    protected val logger = LogManager.getLogger(this.javaClass)

    /** Executes the task. */
    abstract operator fun invoke(input: I): O;

    /**
     * Makes a thread for this task.
     *
     * @param[destination] The task where to pipe this one into.  If `null`, the task's output is discarded.
     * @param[start] Whether to start the thread immediately.
     * @return a thread and queue for running this task.
     */
    fun thread(destination: ThreadedTask<O, *>?, start: Boolean = true): ThreadedTask<I, O> {
        val threadLogger = LogManager.getLogger("${logger.name}.thread")

        val inputQueue = LinkedBlockingQueue<I>()
        val thread = thread(name = this.javaClass.baseName(), start = start) {
            threadLogger.debug("Starting task thread")
            try {
                while (true) {
                    val input = inputQueue.take()!!
                    if (input == sentinel) {
                        threadLogger.trace("Reached input sentinel")
                        destination?.run { queue.put(destination.task.sentinel) }
                        break
                    }
                    threadLogger.trace("Processing input item")
                    try {
                        val result = this@ThreadableTask(input)
                        destination?.run {
                            threadLogger.trace("Queueing result for {}", task.javaClass.baseName())
                            queue.put(result)
                        }
                    } catch (e: TransientError) {
                        threadLogger.catching(e.cause)
                    }
                }
            } catch (e: Exception) {
                threadLogger.catching(e)
                throw e
            } finally {
                threadLogger.debug("Closing task")
                this.close()
                threadLogger.trace("Task finished")
            }
        }
        return ThreadedTask(this, thread, inputQueue)
    }
}


class ProgressReporter(val taskSize: Int, val callback: (Int, Int) -> Unit) : ThreadableTask<Boolean, Unit>(true) {
    /** How many items have been completed so far. */
    private var completed = 0

    /**
     * Updates the progress of a task pipeline.
     *
     * @param[input] Unused.
     */
    override operator fun invoke(input: Boolean) {
        ++completed
        logger.trace("Progress status: {}/{}", completed, taskSize)
        callback(completed, taskSize)
    }

    override fun close() = Unit
}


/**
 * A transient task error.  The task may proceed with the next input.
 */
class TransientError(cause: Throwable) : Exception(cause) {
    companion object {
        /**
         * Executes a code block, wrapping any [Exception] thrown into
         * a [TransientError].
         */
        inline fun<T> wrap (block: () -> T): T {
            try {
                return block()
            } catch (e: Exception) {
                throw TransientError(e)
            }
        }
    }
}
