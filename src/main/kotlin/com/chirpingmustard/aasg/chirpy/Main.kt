package com.chirpingmustard.aasg.chirpy

import com.chirpingmustard.aasg.chirpy.cli.ChirpyCommand
import com.chirpingmustard.aasg.chirpy.cli.CommandBase
import com.chirpingmustard.aasg.chirpy.cli.HelpCommand
import com.github.rvesse.airline.Cli
import com.github.rvesse.airline.model.OptionMetadata
import com.github.rvesse.airline.parser.command.CliParser
import org.reflections.Reflections
import java.io.File


/**
 * Configure Log4j, looking at the following:
 * - A `CHIRPY_LOG4J` environment variable;
 * - A `chirpy-log4j.*` file in the current working directory.
 */
private fun setupLogging() {
    // If the log4j config file has already been set, bail out.
    if (System.getProperty("log4j.configurationFile") != null) {
        return
    }

    // Try the environment variable first.
    val env = System.getenv("CHIRPY_LOG4J")
    if (env != null && File(env).isFile) {
        System.setProperty("log4j.configurationFile", env)
        return
    }

    // Next, try a local file.
    val local = File(".").listFiles { dir, name -> name.startsWith("chirpy-log4j.") }?.firstOrNull()
    if (local != null && local.isFile) {
        System.setProperty("log4j.configurationFile", local.toString())
        return
    }
}


/**
 * Global command line state.
 */
object CommandLine {
    /**
     * Names of parsed command-line options.
     */
    internal val seenNames = mutableSetOf<String>()

    /**
     * Checks whether a given command-line option was passed.
     *
     * @param[name]  Full name (as declared) of the command-line option to
     *               check for.
     * @return  Whether the given command-line option is present or not.
     */
    fun has(name: String): Boolean {
        return name in seenNames
    }
}


fun main(args: Array<String>) {
    // First of all, set up log4j.
    setupLogging()

    // Then prepare the command-line parser.
    val builder = Cli.builder<CommandBase>("chirpy")
            .withDescription("the stupid content tracker")
            .withDefaultCommand(HelpCommand::class.java)
            .withCommand(HelpCommand::class.java)
            .withCommands(Reflections().getSubTypesOf(ChirpyCommand::class.java))
    val parser = builder.build()
    // Make the list of parsed command-line options available.
    object : CliParser<CommandBase>() {
        init {
            val state = tryParse(parser.metadata, args.asIterable())
            for (option in state.parsedOptions) {
                CommandLine.seenNames.addAll(option.left.options)
            }
        }
    }

    // And off we go!
    val command = parser.parse(*args)
    if (command is ChirpyCommand && (command.help.showHelpIfRequested() ||
                                     command.showVersionIfRequested())) {
        return
    }
    command()
}
