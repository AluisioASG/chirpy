import module namespace progress = 'com.chirpingmustard.aasg.chirpy.redundifier.ExportProgress';


(: BEGIN SHARED CODE :)

declare variable $bbcode-stylesheet-text external := file:read-text('bbcode.xsl');
declare variable $bbcode-stylesheet := parse-xml($bbcode-stylesheet-text);

declare variable $epoch := function($dt as xs:dateTime) as xs:numeric {
(: https://stackoverflow.com/a/3468123 :)
	($dt - xs:dateTime('1970-01-01T00:00:00Z')) div xs:dayTimeDuration('PT1S')
};

declare variable $flag-bbcode := function($message as node()*) {
	let $links := $message/bb-url
	let $magic-urls := $links[(text() = @href) or
			(count(node()) = 1 and
					contains(text(), ' ... ') and
					starts-with(@href, substring-before(text(), ' ... ')) and
					ends-with(@href, substring-after(text(), ' ... ')))]
	let $non-magic-urls := $links except $magic-urls

	let $imgs := $message//bb-img
	let $smileys := $imgs[@smileyFor]
	let $non-smileys := $imgs except $smileys

	let $has-bbcode := exists($non-magic-urls) or
			exists($non-smileys) or
			exists($message/*[not(contains('br img', local-name()))])
	return ($has-bbcode, exists($smileys), exists($magic-urls))
};

declare variable $to-bbcode := function($message as node()*) as xs:string {
(: U+E00A: private-use character used as a substitute for U+000A which we cannot seem to output. :)
	replace(xslt:transform-text($message, $bbcode-stylesheet), "&#xe00a;", "&#xa;")
};

(: END SHARED CODE :)


declare variable $authors := /author;

prof:void((progress:setInstance(), progress:maxHint(xs:int(count(/post))))),
<forum-message-list>
	<topic forum-name="Individual XKCD Comic Threads" title='1190: "Time"'>
	{for $p in /post
		let $msg := $p/message
		let $flags := $flag-bbcode($msg)

		let $taskProgress := progress:step()
		return <message id="{substring($p/@id, 2)}" title="{$p/subject}" posted="{$epoch(xs:dateTime(replace($p/date, 'Z', ':00Z')))}" by="{$authors[@id = $p/author/@ref]/username}" bbcode="{number($flags[1])}" smiley="{number($flags[2])}" magic-url="{number($flags[3])}" signature="{number(exists($p/hasSignature))}" xml:space="preserve">
			{$to-bbcode($msg)}
		</message>
	}
	</topic>
</forum-message-list>
