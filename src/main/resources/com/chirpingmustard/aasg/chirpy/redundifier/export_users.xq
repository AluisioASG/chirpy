import module namespace progress = 'com.chirpingmustard.aasg.chirpy.redundifier.ExportProgress';


(: BEGIN SHARED CODE :)

declare variable $bbcode-stylesheet-text external := file:read-text('bbcode.xsl');
declare variable $bbcode-stylesheet := parse-xml($bbcode-stylesheet-text);

declare variable $epoch := function($dt as xs:dateTime) as xs:numeric {
	(: https://stackoverflow.com/a/3468123 :)
	($dt - xs:dateTime('1970-01-01T00:00:00Z')) div xs:dayTimeDuration('PT1S')
};

declare variable $flag-bbcode := function($message as node()*) {
	let $links := $message/bb-url
	let $magic-urls := $links[(text() = @href) or
			(count(node()) = 1 and
					contains(text(), ' ... ') and
					starts-with(@href, substring-before(text(), ' ... ')) and
					ends-with(@href, substring-after(text(), ' ... ')))]
	let $non-magic-urls := $links except $magic-urls

	let $imgs := $message//bb-img
	let $smileys := $imgs[@smileyFor]
	let $non-smileys := $imgs except $smileys

	let $has-bbcode := exists($non-magic-urls) or
			exists($non-smileys) or
			exists($message/*[not(contains('br img', local-name()))])
	return ($has-bbcode, exists($smileys), exists($magic-urls))
};

declare variable $to-bbcode := function($message as node()*) as xs:string {
	(: U+E00A: private-use character used as a substitute for U+000A which we cannot seem to output. :)
	replace(xslt:transform-text($message, $bbcode-stylesheet), "&#xe00a;", "&#xa;")
};

(: END SHARED CODE :)


declare variable $make-salt := function($size as xs:integer) as xs:string {
	substring(string(bin:hex(translate(random:uuid(), '-', ''))), 1, $size)
};

declare variable $emailfy-username := function($salt as xs:string, $username as xs:string) as xs:string {
	let $str := concat($salt, '$', $username)
	return translate(string(xs:base64Binary(hash:sha1($str))), '+/=', '-_')
};

declare variable $hash-password := function($password as xs:string) as xs:string {
	let $salt := $make-salt(8)
	let $hash := translate(string(xs:base64Binary(hash:md5($password))), '=', '')
	return concat('$H$', $salt, '$', $hash)
};


prof:void((progress:setInstance(), progress:maxHint(xs:int(count(/author))))),
<users>
{for $u in /author
	let $username := string($u/username)
	let $email := concat($emailfy-username($make-salt(12), $username), '@sfr.chirpingmustard.com')
	let $signature-bbflags := if ($u/signature) then
		$flag-bbcode($u/signature)
	else ()
	let $signature := if ($u/signature) then (
		<user_signature>{$to-bbcode($u/signature)}</user_signature>,
		<user_signature_bbcode>{number($signature-bbflags[1])}</user_signature_bbcode>,
		<user_signature_smilies>{number($signature-bbflags[2])}</user_signature_smilies>,
		<user_signature_urls>{number($signature-bbflags[3])}</user_signature_urls>
	) else ()
	let $taskProgress := progress:step()
	return <user>
		<user_id>{substring($u/@id, 2)}</user_id>
		<user_ip></user_ip>
		<user_regdate>{$epoch(xs:dateTime(replace($u/joined, 'Z', ':00Z')))}</user_regdate>
		<username>{$username}</username>
		<user_email>{$email}</user_email>
		<user_birthday></user_birthday>
		<user_password>{$hash-password($email)}</user_password>
		{$signature}
	</user>
}
</users>
