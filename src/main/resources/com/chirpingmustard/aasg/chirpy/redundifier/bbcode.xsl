<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
>
	<xsl:output method="text" encoding="UTF-8"/>

	<xsl:template match="br">
		<xsl:text>&#xE00A;</xsl:text>
	</xsl:template>

	<!-- Positioning blocks -->
	<xsl:template match="bb-center">
		<xsl:text>[center]</xsl:text><xsl:apply-templates/><xsl:text>[/center]</xsl:text>
	</xsl:template>
	<xsl:template match="bb-right">
		<xsl:text>[right]</xsl:text><xsl:apply-templates/><xsl:text>[/right]</xsl:text>
	</xsl:template>

	<!-- Links -->
	<xsl:template match="bb-img">
		<xsl:text>[img]</xsl:text><xsl:value-of select="@src"/><xsl:text>[/img]</xsl:text>
	</xsl:template>
	<xsl:template match="bb-url">
		<xsl:choose>
			<xsl:when test="(text() = @href) or (contains(text(), ' ... ') and starts-with(@href, substring-before(text(), ' ... ')) and substring(@href, string-length(@href) - string-length(substring-after(text(), ' ... ')) + 1) = substring-after(text(), ' ... '))">
				<xsl:value-of select="@href"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>[url=</xsl:text><xsl:value-of select="@href"/><xsl:text>]</xsl:text><xsl:apply-templates/><xsl:text>[/url]</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- Magic URLs -->
	<xsl:template match="bb-url[text() = @href]">
		<xsl:value-of select="@href"/>
	</xsl:template>

	<!-- Parameter-less inline text formatting -->
	<xsl:template match="bb-b">
		<xsl:text>[b]</xsl:text><xsl:apply-templates/><xsl:text>[/b]</xsl:text>
	</xsl:template>
	<xsl:template match="bb-i">
		<xsl:text>[i]</xsl:text><xsl:apply-templates/><xsl:text>[/i]</xsl:text>
	</xsl:template>
	<xsl:template match="bb-u">
		<xsl:text>[u]</xsl:text><xsl:apply-templates/><xsl:text>[/u]</xsl:text>
	</xsl:template>
	<xsl:template match="bb-s">
		<xsl:text>[s]</xsl:text><xsl:apply-templates/><xsl:text>[/s]</xsl:text>
	</xsl:template>
	<xsl:template match="bb-sup">
		<xsl:text>[sup]</xsl:text><xsl:apply-templates/><xsl:text>[/sup]</xsl:text>
	</xsl:template>
	<xsl:template match="bb-sub">
		<xsl:text>[sub]</xsl:text><xsl:apply-templates/><xsl:text>[/sub]</xsl:text>
	</xsl:template>

	<!-- Parametrized inline text formatting -->
	<xsl:template match="bb-size">
		<xsl:text>[size=</xsl:text><xsl:value-of select="@size"/><xsl:text>]</xsl:text><xsl:apply-templates/><xsl:text>[/size]</xsl:text>
	</xsl:template>
	<xsl:template match="bb-color">
		<xsl:text>[color=</xsl:text><xsl:value-of select="@color"/><xsl:text>]</xsl:text><xsl:apply-templates/><xsl:text>[/color]</xsl:text>
	</xsl:template>

	<!-- Lists -->
	<xsl:template match="bb-list[@type='unordered']">
		<xsl:text>[list]</xsl:text><xsl:apply-templates/><xsl:text>[/list]</xsl:text>
	</xsl:template>
	<xsl:template match="bb-list[@type='ordered']">
		<xsl:text>[list=1]</xsl:text><xsl:apply-templates/><xsl:text>[/list]</xsl:text>
	</xsl:template>
	<xsl:template match="bb-li">
		<xsl:text>[*]</xsl:text><xsl:apply-templates/>
	</xsl:template>

	<!-- Containers -->
	<xsl:template match="bb-quote[not(@cite)]">
		<xsl:text>[quote]</xsl:text><xsl:apply-templates/><xsl:text>[/quote]</xsl:text>
	</xsl:template>
	<xsl:template match="bb-quote">
		<xsl:text>[quote="</xsl:text><xsl:value-of select="@cite"/><xsl:text>"]</xsl:text><xsl:apply-templates/><xsl:text>[/quote]</xsl:text>
	</xsl:template>
	<xsl:template match="bb-spoiler">
		<xsl:text>[spoiler]</xsl:text><xsl:apply-templates/><xsl:text>[/spoiler]</xsl:text>
	</xsl:template>
	<xsl:template match="bb-code">
		<xsl:text>[code]</xsl:text><xsl:apply-templates/><xsl:text>[/code]</xsl:text>
	</xsl:template>
	<xsl:template match="attachment">
		<!--attachment id="{$id}" thumbnailed="{$thumbnailed}" originalFilename="{$filename}" filesize="{$filesize}" views="{$viewcount}">
			<xsl:apply-templates select="$comment-node/text()" mode="bbcode-attachment"/>
		</attachment-->
	</xsl:template>

	<!-- Smilies -->
	<xsl:template match="bb-img[@smileyFor]">
		<xsl:value-of select="@smileyFor"/>
	</xsl:template>

</xsl:stylesheet>
