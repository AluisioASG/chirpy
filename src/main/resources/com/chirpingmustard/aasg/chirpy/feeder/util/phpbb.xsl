<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" extension-element-prefixes="sfrsz sfrht" exclude-result-prefixes="fn"
                xpath-default-namespace="http://www.w3.org/1999/xhtml"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:fn="http://www.w3.org/2005/xpath-functions"
                xmlns:sfrsz="https://aasg.chirpingmustard.com/sfr/strings"
                xmlns:sfrbb="https://aasg.chirpingmustard.com/sfr/phpbb"
                xmlns:sfrht="https://aasg.chirpingmustard.com/sfr/html"
>
	<xsl:import href="strings.xsl"/>

	<!-- **Ordered** list of month name abbreviations. -->
	<sfrbb:month-abbrevs>
		<sfrbb:month-abbrev>Jan</sfrbb:month-abbrev>
		<sfrbb:month-abbrev>Feb</sfrbb:month-abbrev>
		<sfrbb:month-abbrev>Mar</sfrbb:month-abbrev>
		<sfrbb:month-abbrev>Apr</sfrbb:month-abbrev>
		<sfrbb:month-abbrev>May</sfrbb:month-abbrev>
		<sfrbb:month-abbrev>Jun</sfrbb:month-abbrev>
		<sfrbb:month-abbrev>Jul</sfrbb:month-abbrev>
		<sfrbb:month-abbrev>Aug</sfrbb:month-abbrev>
		<sfrbb:month-abbrev>Sep</sfrbb:month-abbrev>
		<sfrbb:month-abbrev>Oct</sfrbb:month-abbrev>
		<sfrbb:month-abbrev>Nov</sfrbb:month-abbrev>
		<sfrbb:month-abbrev>Dec</sfrbb:month-abbrev>
	</sfrbb:month-abbrevs>

	<!-- Converts a date in phpBB's default format (`%a %b %d, %Y %I:%M %p %Z`) to ISO 8601 format (`%Y-%m-%dT%H:%M` + `Z` if timezone is `UTC`).  NOTE that this format is not a lexically valid `xsd:dateTime` value, since it's missing the seconds. -->
	<xsl:function name="sfrbb:date-to-iso" as="xs:string">
		<xsl:param name="date-time" as="xs:string"/>

		<xsl:variable name="dateparts" select="tokenize(normalize-space($date-time), '[:, ]+')"/>
		<xsl:variable name="month-number"
		              select="count(document('phpbb.xsl')//sfrbb:month-abbrevs/sfrbb:month-abbrev[.=$dateparts[2]]/preceding-sibling::*) + 1"/>

		<xsl:variable name="result">
			<xsl:value-of select="$dateparts[4]"/>
			<xsl:text>-</xsl:text>
			<xsl:value-of select="format-number($month-number, '00')"/>
			<xsl:text>-</xsl:text>
			<xsl:value-of select="$dateparts[3]"/>
			<xsl:text>T</xsl:text>
			<xsl:choose>
				<xsl:when test="$dateparts[7] = 'am'">
					<xsl:value-of select="format-number(number($dateparts[5]) mod 12, '00')"/>
				</xsl:when>
				<xsl:when test="$dateparts[7] = 'pm'">
					<xsl:value-of select="format-number((number($dateparts[5]) mod 12) + 12, '00')"/>
				</xsl:when>
			</xsl:choose>
			<xsl:text>:</xsl:text>
			<xsl:value-of select="$dateparts[6]"/>
			<xsl:if test="$dateparts[8] = 'UTC'">
				<xsl:text>Z</xsl:text>
			</xsl:if>
		</xsl:variable>
		<xsl:sequence select="string($result)"/>
	</xsl:function>

	<!-- Returns the object ID for the user who authored a post. -->
	<xsl:function name="sfrbb:post-author-id" as="xs:string">
		<xsl:param name="post"/>

		<xsl:variable name="profile-url" select="$post//dl[sfrht:classed(., 'postprofile')]/dt/a[last()]/@href"/>
		<xsl:sequence select="concat('u', sfrsz:url-param($profile-url, 'u'))"/>
	</xsl:function>
</xsl:stylesheet>
