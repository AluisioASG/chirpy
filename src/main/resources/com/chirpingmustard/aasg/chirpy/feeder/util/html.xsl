<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" exclude-result-prefixes="fn"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:fn="http://www.w3.org/2005/xpath-functions"
                xmlns:sfrht="https://aasg.chirpingmustard.com/sfr/html"
>

	<xsl:function name="sfrht:classed" as="xs:boolean">
		<xsl:param name="context"/>
		<xsl:param name="class-name" as="xs:string"/>
		<xsl:sequence select="tokenize($context/@class, '\s+') = $class-name"/>
	</xsl:function>

	<xsl:function name="sfrht:styled" as="xs:boolean">
		<xsl:param name="context"/>
		<xsl:param name="property" as="xs:string"/>
		<xsl:param name="value" as="xs:string"/>

		<xsl:variable name="property-value" select="sfrht:style($context, $property)"/>
		<xsl:choose>
			<xsl:when test="$value = '*'">
				<xsl:sequence select="$property-value != ''"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:sequence select="$property-value = normalize-space($value)"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:function>

	<xsl:function name="sfrht:style" as="xs:string">
		<xsl:param name="context"/>
		<xsl:param name="property" as="xs:string"/>

		<xsl:variable name="styles" select="tokenize($context/@style, ';')"/>
		<!-- Missing CSS whitespace char U+000C FORM FEED.  XML 1.1 allows it. -->
		<xsl:variable name="decl"
		              select="$styles[starts-with(translate(., ' &#x9;&#xa;&#xd;', ''), concat($property, ':'))]"/>
		<xsl:sequence select="normalize-space(substring-after($decl, ':'))"/>
	</xsl:function>

</xsl:stylesheet>
