<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" extension-element-prefixes="sfrht sfrsz" exclude-result-prefixes="fn"
                xpath-default-namespace="http://www.w3.org/1999/xhtml"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fn="http://www.w3.org/2005/xpath-functions"
                xmlns:sfrht="https://aasg.chirpingmustard.com/sfr/html"
                xmlns:sfrsz="https://aasg.chirpingmustard.com/sfr/strings"
>
	<xsl:import href="util/html.xsl"/>
	<xsl:import href="util/strings.xsl"/>
	<xsl:output method="xml" encoding="UTF-8"/>

	<xsl:template match="/*">
		<bb-post>
			<xsl:apply-templates mode="bbcode"/>
		</bb-post>
	</xsl:template>
	<xsl:template match="br" mode="bbcode">
		<br/>
	</xsl:template>

	<!-- Positioning blocks -->
	<xsl:template match="div[@align='center']" mode="bbcode">
		<bb-center>
			<xsl:apply-templates mode="bbcode"/>
		</bb-center>
	</xsl:template>
	<xsl:template match="div[@align='right']" mode="bbcode">
		<bb-right>
			<xsl:apply-templates mode="bbcode"/>
		</bb-right>
	</xsl:template>

	<!-- Links -->
	<xsl:template match="img" mode="bbcode">
		<bb-img src="{sfrsz:remove-url-param(@src, 'sid')}"/>
	</xsl:template>
	<xsl:template match="a[sfrht:classed(., 'postlink')]" mode="bbcode">
		<bb-url href="{sfrsz:remove-url-param(@href, 'sid')}">
			<xsl:apply-templates mode="bbcode"/>
		</bb-url>
	</xsl:template>

	<!-- Parameter-less inline text formatting -->
	<xsl:template match="span[sfrht:styled(., 'font-weight', 'bold')]" mode="bbcode">
		<bb-b>
			<xsl:apply-templates mode="bbcode"/>
		</bb-b>
	</xsl:template>
	<xsl:template match="span[sfrht:styled(., 'font-style', 'italic')]" mode="bbcode">
		<bb-i>
			<xsl:apply-templates mode="bbcode"/>
		</bb-i>
	</xsl:template>
	<xsl:template match="span[sfrht:styled(., 'text-decoration', 'underline')]" mode="bbcode">
		<bb-u>
			<xsl:apply-templates mode="bbcode"/>
		</bb-u>
	</xsl:template>
	<xsl:template match="strike" mode="bbcode">
		<bb-s>
			<xsl:apply-templates mode="bbcode"/>
		</bb-s>
	</xsl:template>
	<xsl:template match="sup" mode="bbcode">
		<bb-sup>
			<xsl:apply-templates mode="bbcode"/>
		</bb-sup>
	</xsl:template>
	<xsl:template match="sub" mode="bbcode">
		<bb-sub>
			<xsl:apply-templates mode="bbcode"/>
		</bb-sub>
	</xsl:template>

	<!-- Parametrized inline text formatting -->
	<xsl:template match="span[sfrht:styled(., 'font-size', '*')]" mode="bbcode">
		<xsl:variable name="arg" select="substring-before(sfrht:style(., 'font-size'), '%')"/>
		<bb-size size="{$arg}">
			<xsl:apply-templates mode="bbcode"/>
		</bb-size>
	</xsl:template>
	<xsl:template match="span[sfrht:styled(., 'color', '*')]" mode="bbcode">
		<xsl:variable name="arg" select="sfrht:style(., 'color')"/>
		<bb-color color="{$arg}">
			<xsl:apply-templates mode="bbcode"/>
		</bb-color>
	</xsl:template>

	<!-- Lists -->
	<xsl:template match="ul" mode="bbcode">
		<bb-list type="unordered">
			<xsl:apply-templates mode="bbcode"/>
		</bb-list>
	</xsl:template>
	<xsl:template match="ol" mode="bbcode">
		<bb-list type="ordered">
			<xsl:apply-templates mode="bbcode"/>
		</bb-list>
	</xsl:template>
	<xsl:template match="li" mode="bbcode">
		<bb-li>
			<xsl:apply-templates mode="bbcode"/>
		</bb-li>
	</xsl:template>

	<!-- Containers -->
	<xsl:template match="blockquote[sfrht:classed(., 'uncited')]" mode="bbcode">
		<bb-quote>
			<xsl:apply-templates select="./div" mode="bbcode"/>
		</bb-quote>
	</xsl:template>
	<xsl:template match="blockquote[not(sfrht:classed(., 'uncited'))]" mode="bbcode">
		<xsl:variable name="arg" select="substring-before(./div/cite[position()=1], ' wrote')"/>
		<bb-quote cite="{$arg}">
			<xsl:apply-templates select="./div/node()[position()!=1]" mode="bbcode"/>
		</bb-quote>
	</xsl:template>
	<xsl:template match="div[./div[sfrht:classed(., 'quotetitle')]/b[text()='Spoiler:']]" mode="bbcode">
		<bb-spoiler>
			<xsl:apply-templates select="./div[sfrht:classed(., 'quotecontent')]/div" mode="bbcode"/>
		</bb-spoiler>
	</xsl:template>
	<xsl:template match="dl[sfrht:classed(., 'codebox')]" mode="bbcode">
		<bb-code>
			<xsl:apply-templates select=".//code" mode="bbcode"/>
		</bb-code>
	</xsl:template>
	<xsl:template match="*[sfrht:classed(., 'inline-attachment')]" mode="bbcode">
		<xsl:apply-templates mode="bbcode-attachment"/>
	</xsl:template>

	<!-- Attachments -->
	<xsl:template match="dl[sfrht:classed(., 'file') and not(./dt[sfrht:classed(., 'attach-image')])]" mode="bbcode-attachment">
		<xsl:call-template name="bbcode-attachment">
			<xsl:with-param name="url" select="dt/a/@href"/>
			<xsl:with-param name="thumbnailed" select="false()"/>
			<xsl:with-param name="filename" select="dt/a/text()"/>
			<xsl:with-param name="description" select="dd[last()]/text()"/>
			<xsl:with-param name="comment-node" select="dd[position()!=last()]/em"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template match="dl[sfrht:classed(., 'file') and dt[sfrht:classed(., 'attach-image')]]" mode="bbcode-attachment">
		<xsl:call-template name="bbcode-attachment">
			<xsl:with-param name="url" select="dt/img/@src"/>
			<xsl:with-param name="thumbnailed" select="false()"/>
			<xsl:with-param name="filename" select="dt/img/@alt"/>
			<xsl:with-param name="description" select="dd[last()]/text()"/>
			<xsl:with-param name="comment-node" select="dd[position()!=last()]/em"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template match="dl[sfrht:classed(., 'thumbnail')]" mode="bbcode-attachment">
		<xsl:call-template name="bbcode-attachment">
			<xsl:with-param name="url" select="dt/a/@href"/>
			<xsl:with-param name="thumbnailed" select="true()"/>
			<xsl:with-param name="filename" select="dt/a/img/@alt"/>
			<xsl:with-param name="description" select="dt/a/img/@title"/>
			<xsl:with-param name="comment-node" select="dd"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template match="text()" mode="bbcode-attachment"></xsl:template>

	<xsl:template name="bbcode-attachment">
		<xsl:param name="url"/>
		<xsl:param name="thumbnailed"/>
		<xsl:param name="filename"/>
		<xsl:param name="description"/>
		<xsl:param name="comment-node"/>

		<xsl:variable name="id" select="sfrsz:url-param($url, 'id')"/>
		<xsl:variable name="desc-tokens" select="tokenize($description, '\s+')"/>
		<xsl:variable name="viewcount">
			<xsl:choose>
				<xsl:when test="number($desc-tokens[last() - 1]) = number($desc-tokens[last() - 1])">
					<xsl:value-of select="$desc-tokens[last() - 1]"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="0"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="filesize-container" select="concat($desc-tokens[last() - 4], ' ', $desc-tokens[last() - 3])"/>
		<xsl:variable name="filesize" select="substring($filesize-container, 2, string-length($filesize-container) - 2)"/>

		<attachment id="{$id}" thumbnailed="{$thumbnailed}" originalFilename="{$filename}" filesize="{$filesize}"
		            views="{$viewcount}">
			<xsl:apply-templates select="$comment-node/text()" mode="bbcode-attachment"/>
		</attachment>
	</xsl:template>

	<!-- Smilies -->
	<xsl:template match="img[not(contains(@src, '://')) and @alt != 'Image' and @title != '']" mode="bbcode">
		<bb-img src="{resolve-uri(sfrsz:remove-url-param(@src, 'sid'), base-uri(.))}" smileyFor="{@alt}"/>
	</xsl:template>

</xsl:stylesheet>
