<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" extension-element-prefixes="sfrbb sfrht" exclude-result-prefixes="fn"
                xpath-default-namespace="http://www.w3.org/1999/xhtml"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fn="http://www.w3.org/2005/xpath-functions"
                xmlns:sfrbb="https://aasg.chirpingmustard.com/sfr/phpbb"
                xmlns:sfrht="https://aasg.chirpingmustard.com/sfr/html"
>
	<xsl:import href="util/html.xsl"/>
	<xsl:import href="util/phpbb.xsl"/>
	<xsl:import href="bbcode.xsl"/>
	<xsl:output method="xml" encoding="UTF-8"/>

	<xsl:template match="/*">
		<xsl:call-template name="postauthor">
			<xsl:with-param name="post" select="."/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template match="text()" mode="postauthor"></xsl:template>
	<xsl:template match="text()" mode="postauthor-profile"></xsl:template>

	<xsl:template name="postauthor">
		<xsl:param name="post"/>

		<author id="{sfrbb:post-author-id($post)}">
			<xsl:apply-templates select="$post" mode="postauthor"/>
		</author>
	</xsl:template>

	<xsl:template match="dl[sfrht:classed(., 'postprofile')]" mode="postauthor">
		<xsl:apply-templates mode="postauthor-profile"/>
	</xsl:template>
	<xsl:template match="div[sfrht:classed(., 'signature')]" mode="postauthor">
		<signature xml:space="preserve"><xsl:apply-templates mode="bbcode"/></signature>
	</xsl:template>

	<xsl:template match="dt/a[last()]" mode="postauthor-profile">
		<username>
			<xsl:value-of select="."/>
		</username>
	</xsl:template>
	<xsl:template match="img[@alt='User avatar']" mode="postauthor-profile">
		<avatar>
			<xsl:value-of select="resolve-uri(@src, base-uri(.))"/>
		</avatar>
	</xsl:template>
	<xsl:template match="dd[strong[text()='Joined:']]" mode="postauthor-profile">
		<joined>
			<xsl:value-of select="sfrbb:date-to-iso(text())"/>
		</joined>
	</xsl:template>
	<xsl:template match="dd[strong[text()='Posts:']]" mode="postauthor-profile">
		<posts>
			<xsl:value-of select="normalize-space(text())"/>
		</posts>
	</xsl:template>
	<xsl:template match="dd[strong[text()='Location:']]" mode="postauthor-profile">
		<location>
			<xsl:value-of select="normalize-space(text())"/>
		</location>
	</xsl:template>

</xsl:stylesheet>
