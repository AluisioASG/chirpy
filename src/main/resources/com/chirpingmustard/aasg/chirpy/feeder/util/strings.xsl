<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" exclude-result-prefixes="fn"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:fn="http://www.w3.org/2005/xpath-functions"
                xmlns:sfrsz="https://aasg.chirpingmustard.com/sfr/strings"
>

	<!-- Extracts a query parameter from a URL. -->
	<xsl:function name="sfrsz:url-param" as="xs:string">
		<xsl:param name="url" as="xs:anyURI"/>
		<xsl:param name="param-name" as="xs:string"/>

		<xsl:variable name="q" select="tokenize(substring-after($url, '?'), '[;&amp;]')"/>
		<xsl:sequence select="substring-after($q[starts-with(., concat($param-name, '='))], '=')"/>
	</xsl:function>

	<!-- Removes a query parameter from a URL. -->
	<xsl:function name="sfrsz:remove-url-param" as="xs:string">
		<xsl:param name="url" as="xs:string"/> <!-- Should be xs:anyURI, but p3891624 has an invalid <a href>. -->
		<xsl:param name="param-name" as="xs:string"/>

		<xsl:variable name="match" select="concat('([?;&amp;])', $param-name, '=.+?[;&amp;]?(#?)')"/>
		<xsl:sequence select="replace($url, $match, '$1$2')"/>
	</xsl:function>

</xsl:stylesheet>
