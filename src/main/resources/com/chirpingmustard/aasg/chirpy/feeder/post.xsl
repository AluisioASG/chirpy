<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" extension-element-prefixes="sfrbb sfrsz sfrht" exclude-result-prefixes="fn"
                xpath-default-namespace="http://www.w3.org/1999/xhtml"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fn="http://www.w3.org/2005/xpath-functions"
                xmlns:sfrbb="https://aasg.chirpingmustard.com/sfr/phpbb"
                xmlns:sfrsz="https://aasg.chirpingmustard.com/sfr/strings"
                xmlns:sfrht="https://aasg.chirpingmustard.com/sfr/html"
>
	<xsl:import href="util/html.xsl"/>
	<xsl:import href="util/phpbb.xsl"/>
	<xsl:import href="bbcode.xsl"/>
	<xsl:output method="xml" encoding="UTF-8"/>

	<xsl:template match="/*">
		<xsl:call-template name="postdata">
			<xsl:with-param name="post" select="."/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template match="text()" mode="post"></xsl:template>
	<xsl:template match="text()" mode="postbody"></xsl:template>
	<xsl:template match="text()" mode="postprofile"></xsl:template>

	<xsl:template name="postdata">
		<xsl:param name="post"/>

		<post id="{$post/@id}">
			<xsl:apply-templates select="$post" mode="post"/>
		</post>
	</xsl:template>

	<xsl:template match="div[sfrht:classed(., 'postbody')]" mode="post">
		<xsl:apply-templates mode="postbody"/>
	</xsl:template>
	<xsl:template match="dl[sfrht:classed(., 'postprofile')]/dt/a[last()]" mode="post">
		<author ref="u{sfrsz:url-param(@href, 'u')}"/>
	</xsl:template>

	<xsl:template match="h3[1]" mode="postbody">
		<subject xml:space="preserve"><xsl:value-of select="."/></subject>
	</xsl:template>
	<xsl:template match="p[sfrht:classed(., 'author')]" mode="postbody">
		<xsl:variable name="author" select="substring-before(substring-after(., 'by '), ' »')"/>
		<xsl:variable name="date" select="substring-after(., '» ')"/>
		<date>
			<xsl:value-of select="sfrbb:date-to-iso($date)"/>
		</date>
	</xsl:template>

	<xsl:template match="div[sfrht:classed(., 'content')]" mode="postbody">
		<message xml:space="preserve"><xsl:apply-templates mode="bbcode"/></message>
		<xsl:apply-templates select="dl[sfrht:classed(., 'attachbox')]" mode="bbcode-attachment"/>
	</xsl:template>
	<xsl:template match="div[sfrht:classed(., 'signature')]" mode="postbody">
		<hasSignature/>
	</xsl:template>
	<xsl:template match="div[sfrht:classed(., 'notice')]" mode="postbody">
		<xsl:variable name="editor" select="a"/>
		<xsl:variable name="edit-date"
		              select="substring-before(substring-after(a/following-sibling::text()[1], 'on '), ' edited')"/>
		<xsl:variable name="edit-count" select="tokenize(substring-after(., ', edited '), '\s+')[1]"/>
		<edits>
			<timesEdited>
				<xsl:value-of select="$edit-count"/>
			</timesEdited>
			<lastEditedBy>
				<xsl:value-of select="$editor"/>
			</lastEditedBy>
			<lastEditedAt>
				<xsl:value-of select="sfrbb:date-to-iso($edit-date)"/>
			</lastEditedAt>
		</edits>
	</xsl:template>

</xsl:stylesheet>
