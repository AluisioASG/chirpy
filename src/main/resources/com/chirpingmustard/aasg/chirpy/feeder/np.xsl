<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" extension-element-prefixes="sfrbb sfrht"
                xpath-default-namespace="http://www.w3.org/1999/xhtml"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:sfr="https://aasg.chirpingmustard.com/sfr/"
                xmlns:sfrbb="https://aasg.chirpingmustard.com/sfr/phpbb"
                xmlns:sfrht="https://aasg.chirpingmustard.com/sfr/html"
>
	<xsl:import href="util/html.xsl"/>
	<xsl:import href="util/phpbb.xsl"/>
	<xsl:import href="post.xsl"/>
	<xsl:import href="postauthor.xsl"/>
	<xsl:output method="xml" encoding="UTF-8"/>

	<!-- Malformed posts to avoid. -->
	<sfr:posts-to-skip>
		<sfr:post id="p3331793"/>
		<sfr:post id="p3425599"/>
	</sfr:posts-to-skip>

	<xsl:variable name="page-number" select="(//*[sfrht:classed(., 'pagination')]//strong)[1]"/>

	<xsl:template match="/*">
		<xsl:result-document href="np/{$page-number}">
			<collection id="np{$page-number}">
				<name></name>
				<posts>
					<xsl:apply-templates mode="page"/>
				</posts>
			</collection>
		</xsl:result-document>
	</xsl:template>
	<xsl:template match="text()" mode="page"></xsl:template>

	<xsl:template match="div[sfrht:classed(., 'post')][empty(document('np.xsl')//sfr:posts-to-skip/sfr:post[@id=current()/@id])]" mode="page">
		<xsl:variable name="post-index" select="count(preceding-sibling::div[sfrht:classed(., 'post')]) + 1"/>
		<post id="{@id}" loc="OTT:{$page-number}:{$post-index}"/>
		<xsl:result-document href="p/{substring(@id, 2)}">
			<xsl:call-template name="postdata">
				<xsl:with-param name="post" select="."/>
			</xsl:call-template>
		</xsl:result-document>
		<xsl:result-document href="u/{substring(sfrbb:post-author-id(.), 2)}">
			<xsl:call-template name="postauthor">
				<xsl:with-param name="post" select="."/>
			</xsl:call-template>
		</xsl:result-document>
	</xsl:template>

</xsl:stylesheet>
