<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" extension-element-prefixes="sfrht"
                xpath-default-namespace="http://www.w3.org/1999/xhtml"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:sfrht="https://aasg.chirpingmustard.com/sfr/html"
>
	<xsl:import href="util/html.xsl"/>
	<xsl:output method="xml" encoding="UTF-8"/>

	<xsl:template match="/*">
		<xsl:variable name="pagination" select="(//div[sfrht:classed(., 'pagination')])[1]/ul"/>
		<xsl:result-document href="current" method="text">
			<xsl:sequence select="$pagination/li[sfrht:classed(., 'active')]"/>
		</xsl:result-document>
		<xsl:result-document href="latest" method="text">
			<xsl:sequence select="$pagination/li[last() - 1]"/>
		</xsl:result-document>
	</xsl:template>

</xsl:stylesheet>
